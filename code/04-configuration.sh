#!/bin/bash
timestamp=$1
nginx_version=$2
inst_path=$3
base_path=$4

work_path=$(pwd)

#backup initial configuration if any
cp -fr /etc/nginx/ /tmp/nginx_backup_$timestamp 

rm -f /etc/nginx/*default*
cp $base_path/nginx-$nginx_version/objs/ngx_http_modsecurity_module.so /etc/nginx/modules
mv /etc/nginx/modsec/modsecurity.conf-recommended /etc/nginx/modsec/modsecurity.conf

mkdir -p $inst_path/modsec/rules
unzip $base_path/master.zip -d $inst_path/modsec/rules
cp $work_path/config/nginx.conf /etc/nginx/nginx.conf
cp $work_path/config/modsecurity.conf /etc/nginx/modsec/modsecurity.conf

# Restore from original configuration
cp /tmp/nginx_backup_$timestamp/sites-available /etc/nginx/sites-available
cp /tmp/nginx_backup_$timestamp/sites-enabled /etc/nginx/sites-enabled
cp /tmp/nginx_backup_$timestamp/nginx.conf /etc/nginx/nginx.conf.old

exit
