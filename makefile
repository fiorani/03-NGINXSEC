BASEPATH = ./code

TARGET_prereq   = 01-check_prerequisites.sh
TARGET_download = 02-data_download.sh 
TARGET_compile  = 03-compile.sh 
TARGET_configure= 04-configuration.sh 
TARGET_clean    = 99-clean.sh
TARGET_packetize= 10-debpacket.sh

#timestamp	= $(date +%Y-%m-%d)
timestamp	= 2018-07-25
distro		= ubuntu
codename	= $(lsb_release --codename | cut -f2)
repofile	= /etc/apt/sources.list.d/nginx.repo 
nginx_version	= 1.15.2

base_path	= "/tmp/nginx_sec_$(timestamp)/"
inst_path	= "/etc/nginx"
sbin_path	= "/sbin/nginx"
conf_path	= "$(inst_path)/nginx.conf"
pid_path	= "$(inst_path)/nginx.pid"


default: $(TARGET)
	@echo "choose target among the following:" 
	@echo "1) nginxsec" 
	@echo "2) prereq" 
	@echo "3) download" 
	@echo "4) compile" 
	@echo "5) clean" 
	@echo "6) packetize" 

prereq:
	bash $(BASEPATH)/$(TARGET_prereq) $(timestamp) $(distro) $(codename) $(repofile)

download:
	bash $(BASEPATH)/$(TARGET_download) $(timestamp) $(nginx_version) $(base_path) 

compile:
	bash $(BASEPATH)/$(TARGET_compile) $(timestamp) $(nginx_version) $(inst_path) $(sbin_path) $(conf_path) $(pid_path) 

configure:
	bash $(BASEPATH)/$(TARGET_configure) $(timestamp) $(nginx_version) $(inst_path) $(base_path) 

#packet:
#	bash $(BASEPATH)/$(TARGET_packetize)
#	lintian ./packets/nginxsec.deb

clean:
	killall nginx
	bash $(BASEPATH)/$(TARGET_clean)

nginxsec:
	make prereq
	make download
	make compile
	make configure
	#make packet

commit: 
	git config --global user.email "marco.fiorani@eurecom.fr"
	git config --global user.name "marco.fiorani"
	git config --global push.default simple
	git add -A .
	git commit -m "$(TIMESTAMP) - commit from makefile"
	git push

.PHONY: default

