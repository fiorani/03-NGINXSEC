# Instructions for nginxsec

## WHAT:
This makefile and script collection install/remove on your system:
1. The nginx web server on your system.
2. The modwecurity Web Application Firewall.
3. The Owasp Rules coreset.
4. Everything is supposed to be working right out of the box.
5. NGINX is installed in /etc/nginx.
6. Modsec logs are in /var/log/modsec.
7. After installing remember to configure your /etc/nginx.conf file.
8. Please note that several third party libraries are installed during the process, and a software repository is added.
9. Before running customize the makefile with your desired NGINX, Modsecurity, and Owasp Rules version.
10. Right now only Ubuntu 16.04 is supported as a build system. Other ubuntu should be compatible as long as the source repository for 3rd party libraries are added. Need to make the scripts APT-agnostic to make it compatible with other linuxes/unixes.

## HOW TO:
###### As root, or with sudo privileges, execute the command:
> $ make nginxsec

###### To remove, execute the command:
> $ make clean

## TO DO:
1. Preserve existing configuration files if any.
2. Make the installation work on all linux systems not only ubuntu.
3. Create adequate packages for APT and YUM.
4. Write custom rules to detect NoSql injections.
5. Write a tool to analyze modsecurity logs and send notifications of recorded attacks.
